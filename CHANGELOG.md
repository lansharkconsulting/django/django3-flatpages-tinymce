# Change Log

## v0.2.2

1. Fix external_link_list_url value

## v0.2.1

1. Update README.md
1. Add CHANGELOG.md

## v0.2.0

1. Fork and create package compatible with Django >=3.0 and python 3.7
